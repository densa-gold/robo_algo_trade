<?php
require_once 'db.php';
//error_reporting(0);

    function account()
    {
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `configure`");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $token = sprintf($rows[0] ['token']);

        //echo($token)
        //setup the request, you can also use CURLOPT_URL
        $ch = curl_init('https://api-fxpractice.oanda.com/v3/accounts');
        // Returns the data/output as a string instead of raw data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Set your auth headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));
        // get stringified data/output. See CURLOPT_RETURNTRANSFER
        $data = curl_exec($ch);
        // get info about the request
        // close curl resource to free up system resources
        curl_close($ch);
        $json_string = $data;
        $jsondata = ($json_string);
        $obj = json_decode($jsondata, true);
        $account = $obj['accounts'][1]['id'];
        return $account;
    }

    function price_now()
    {
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `configure`");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $token = sprintf($rows[0] ['token']);
        $ticker = sprintf($rows[0] ['ticker']);
        //setup the request, you can also use CURLOPT_URL
        $ch = curl_init();
        // Returns the data/output as a string instead of raw data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Set your auth headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));
        curl_setopt($ch, CURLOPT_URL, "https://api-fxpractice.oanda.com/v3/instruments/" . $ticker . "/candles?count=1&price=M&granularity=S5");
        // get stringified data/output. See CURLOPT_RETURNTRANSFER
        $data = curl_exec($ch);
        // get info about the request
        // close curl resource to free up system resources
        curl_close($ch);
        $json_string = $data;
        $jsondata = ($json_string);
        $obj = json_decode($jsondata, true);
        $last_price = $obj['candles'][0]['mid']['c'];
        return $last_price;
    }

    function get_max_price()
    {
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `configure`");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $token = sprintf($rows[0] ['token']);
        $ticker = sprintf($rows[0] ['ticker']);
        $lasty = date('Y', strtotime("last Year"));
        $fmd = '-01-01T23:59:00.000000';
        $smd = '-12-31T23:59:00.000000';
        $first = ($lasty . $fmd);
        $second = ($lasty . $smd);
        //setup the request, you can also use CURLOPT_URL
        $ch = curl_init();
        // Returns the data/output as a string instead of raw data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Set your auth headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));
        curl_setopt($ch, CURLOPT_URL, "https://api-fxpractice.oanda.com/v3/instruments/" . $ticker . "/candles?&price=A&from=" . $first . "&to=" . $second . "&granularity=D");
        // get stringified data/output. See CURLOPT_RETURNTRANSFER
        $data = curl_exec($ch);
        // get info about the request
        // close curl resource to free up system resources
        curl_close($ch);
        $arr = json_decode($data, true);
        $c = array_map(function ($in) {
            return $in['ask']['c'];
        }, $arr['candles']);
        sort($c);
        //echo 'Min is: '.current($c)."\n";
        $fin = current($c);
        return $fin;
    }

    function get_min_price()
    {
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `configure`");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $token = sprintf($rows[0] ['token']);
        $ticker = sprintf($rows[0] ['ticker']);
        $lasty = date('Y', strtotime("last Year"));
        $fmd = '-01-01T23:59:00.000000';
        $smd = '-12-31T23:59:00.000000';
        $first = ($lasty . $fmd);
        $second = ($lasty . $smd);
        //setup the request, you can also use CURLOPT_URL
        $ch = curl_init();
        // Returns the data/output as a string instead of raw data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Set your auth headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));
        curl_setopt($ch, CURLOPT_URL, "https://api-fxpractice.oanda.com/v3/instruments/" . $ticker . "/candles?&price=A&from=" . $first . "&to=" . $second . "&granularity=D");
        // get stringified data/output. See CURLOPT_RETURNTRANSFER
        $data = curl_exec($ch);
        // get info about the request
        // close curl resource to free up system resources
        curl_close($ch);
        $arr = json_decode($data,true);
        $c = array_map( function ($in){
            return  $in['ask']['c'];
        }, $arr['candles']);
        sort($c);
        //echo 'Min is: '.current($c)."\n";
        $fin = end($c);
        return $fin;
    }

    function get_close_price()
    {
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `configure`");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $token = sprintf($rows[0] ['token']);
        $ticker = sprintf($rows[0] ['ticker']);
        $lasty = date('Y', strtotime("last Year"));
        $fmd = '-01-01T23:59:00.000000';
        $smd = '-12-31T23:59:00.000000';
        $second = ($lasty . $smd);
        //setup the request, you can also use CURLOPT_URL
        $ch = curl_init();
        // Returns the data/output as a string instead of raw data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Set your auth headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));
        curl_setopt($ch, CURLOPT_URL, "https://api-fxpractice.oanda.com/v3/instruments/" . $ticker . "/candles?count=1&price=A&from=" . $second . "&granularity=D");
        // get stringified data/output. See CURLOPT_RETURNTRANSFER
        $data = curl_exec($ch);
        // get info about the request
        // close curl resource to free up system resources
        curl_close($ch);
        $jsondata = ($data);
        $obj = json_decode($jsondata, true);
        $closeprice = $obj['candles'][0]['ask']['c'];
        return $closeprice;

    }

    function write_pivot()
    {
        $now = price_now();
        $close = get_close_price();
        $high = get_max_price();
        $low = get_min_price();
        $pivot = ($high + $low + $close) / 3;
        $r1 = ($pivot + $pivot) - $low;
        $s1 = ($pivot + $pivot) - $high;
        $r2 = $pivot + ($r1 - $s1);
        $s2 = $pivot - ($r1 - $s1);
        $r3 = $high + 2 * ($pivot - $low);
        $s3 = $low - 2 * ($high - $low);
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `configure`");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $ticker = sprintf($rows[0] ['ticker']);
        $datetime = date('Y-m-d h:m:s', strtotime("Now"));
        $result = $mysqli->query("UPDATE `pivot` SET `id`=1,`ticker`='$ticker',`search_datetime`='$datetime',`r3`='$r3',`r2`='$r2',`r1`='$r1',`pivot`='$pivot',`s1`='$s1',`s2`='$s2',`s3`='$s3', `price_now`='$now' WHERE 1");
        if ($result == true){
            echo "success";
        }else{
            echo "error";
        }
    }

    function get_libor_eur(){
        $htmlContent = file_get_contents("https://www.global-rates.com/interest-rates/libor/european-euro/eur-libor-interest-rate-12-months.aspx");
        $DOM = new DOMDocument();
        $DOM->loadHTML($htmlContent);
        $Header = $DOM->getElementsByTagName('td');
        foreach($Header as $NodeHeader)
        {
            $aDataTableHeaderHTML[] = trim($NodeHeader->textContent, '%');

        }
        $month12 = (float)$aDataTableHeaderHTML[76];
        $month11 = (float)$aDataTableHeaderHTML[74];
        $month10 = (float)$aDataTableHeaderHTML[72];
        $month9 = (float)$aDataTableHeaderHTML[70];
        $month8 = (float)$aDataTableHeaderHTML[68];
        $month7 = (float)$aDataTableHeaderHTML[66];
        $month6 = (float)$aDataTableHeaderHTML[64];
        $month5 = (float)$aDataTableHeaderHTML[62];
        $month4 = (float)$aDataTableHeaderHTML[60];
        $month3 = (float)$aDataTableHeaderHTML[58];
        $month2 = (float)$aDataTableHeaderHTML[56];
        $month1 = (float)$aDataTableHeaderHTML[54];
        $stage1=(($month11-$month12)/$month12)*100;
        $stage2=(($month10-$month11)/$month11)*100;
        $stage3=(($month9-$month10)/$month10)*100;
        $stage4=(($month8-$month9)/$month9)*100;
        $stage5=(($month7-$month8)/$month8)*100;
        $stage6=(($month6-$month7)/$month7)*100;
        $stage7=(($month5-$month6)/$month6)*100;
        $stage8=(($month4-$month5)/$month5)*100;
        $stage9=(($month3-$month4)/$month4)*100;
        $stage10=(($month2-$month3)/$month3)*100;
        $stage11=(($month1-$month2)/$month2)*100;
        $sum = $stage1+$stage2+$stage3+$stage4+$stage5+$stage6+$stage7+$stage8+$stage9+$stage10+$stage11;
        return $sum;

    }

    function get_libor_usd(){
        $htmlContent = file_get_contents("https://www.global-rates.com/interest-rates/libor/american-dollar/usd-libor-interest-rate-12-months.aspx");
        $DOM = new DOMDocument();
        $DOM->loadHTML($htmlContent);
        $Header = $DOM->getElementsByTagName('td');
        foreach($Header as $NodeHeader)
        {
            $aDataTableHeaderHTML[] = trim($NodeHeader->textContent, '%');

        }
        $month12 = (float)$aDataTableHeaderHTML[76];
        $month11 = (float)$aDataTableHeaderHTML[74];
        $month10 = (float)$aDataTableHeaderHTML[72];
        $month9 = (float)$aDataTableHeaderHTML[70];
        $month8 = (float)$aDataTableHeaderHTML[68];
        $month7 = (float)$aDataTableHeaderHTML[66];
        $month6 = (float)$aDataTableHeaderHTML[64];
        $month5 = (float)$aDataTableHeaderHTML[62];
        $month4 = (float)$aDataTableHeaderHTML[60];
        $month3 = (float)$aDataTableHeaderHTML[58];
        $month2 = (float)$aDataTableHeaderHTML[56];
        $month1 = (float)$aDataTableHeaderHTML[54];
        $stage1=(($month11-$month12)/$month12)*100;
        $stage2=(($month10-$month11)/$month11)*100;
        $stage3=(($month9-$month10)/$month10)*100;
        $stage4=(($month8-$month9)/$month9)*100;
        $stage5=(($month7-$month8)/$month8)*100;
        $stage6=(($month6-$month7)/$month7)*100;
        $stage7=(($month5-$month6)/$month6)*100;
        $stage8=(($month4-$month5)/$month5)*100;
        $stage9=(($month3-$month4)/$month4)*100;
        $stage10=(($month2-$month3)/$month3)*100;
        $stage11=(($month1-$month2)/$month2)*100;
        $sum = $stage1+$stage2+$stage3+$stage4+$stage5+$stage6+$stage7+$stage8+$stage9+$stage10+$stage11;
        return $sum;

    }

    function write_libor() {
        $tiker_u = 'usd';
        $tiker_e = 'eur';
        $usd = get_libor_usd();
        $eur = get_libor_eur();
        if ($usd > $eur){
            $result = $tiker_u;
        }else{
            $result = $tiker_e;
        }
        $datetime = date('Y-m-d h:m:s', strtotime("Now"));
        global $mysqli;
        $result = $mysqli->query("UPDATE `stratage`.`libor` SET `date_time` = '$datetime', `usd_or_eur` = '$result', `old` = NULL WHERE `id` = 1");
        if ($result == true){
            echo "sucess";
        }else{
            echo "error";
        }
    }

    function update_price()
{
    $pricenow = (float)price_now();
    global $mysqli;
    $datetime = date('Y-m-d h:m:s', strtotime("Now"));
    $result = $mysqli->query("UPDATE `stratage`.`orders` SET `last_update` = '$datetime', `last_update_price` = '$pricenow'");
    if ($result == true) {
        echo "success";
    } else {
        echo "error";
    }
}

    function fallback(){}

    function avg_order(){}

    function create_order(){
        global $mysqli;
        $result1 = $mysqli->query("SELECT * FROM `stratage`.`account_info` LIMIT 0, 1000");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows1[] = $result1->fetch_assoc();
        }
        $result = $mysqli->query("SELECT * FROM `configure`");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $ticker = $rows[0] ['ticker'];
        $token = $rows[0] ['token'];
        $risk = (int)$rows[0] ['max_risk'];
        $balance = (float)$rows1[0] ['balance'];
        $money_management = ($balance / 100) * $risk;
        $price_now = (float)price_now();
        $sum_units = ($money_management * 100000) / $price_now / 1000;
        $account = account();
        $access = check_stage();
        $order_stage = check_stage();
        if($order_stage == 'limit orders'){die;}else{$order_num = $access;}
        //---------------------script die else $order_stage = limit-----------------
        $order_1 = $sum_units / 4;
        $order_2 = $order_1 * 1.5;
        $order_3 = $order_1 + ($order_1 / 2);
        if ($order_num == 1){$units = $order_1;}elseif($order_num == 2){$units = $order_2;}elseif($order_num == 3){$units = $order_3;}
        $updown = generate_trend();
        $count_units = $updown.$units;
        $data = '{
  "order": {
    "units": "'.(int)$count_units.'",
    "instrument": "'.$ticker.'",
    "timeInForce": "FOK",
    "type": "MARKET",
    "positionFill": "DEFAULT"
  }
}';

        $ch = curl_init('https://api-fxpractice.oanda.com/v3/accounts/' . $account . '/orders');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer '. $token));
        $result = curl_exec($ch);
        $json_string = $result;
        $jsondata = ($json_string);
        $obj = json_decode($jsondata, true);
        var_dump($obj);
        $order_id = $obj['orderCreateTransaction']['id'];
        $units = $obj['orderCreateTransaction']['units'];
        $start_price = $obj['orderFillTransaction']['price'];
        $instrument = $obj['orderCreateTransaction']['instrument'];
        $datetime = date('Y-m-d h:m:s', strtotime("Now"));
        $target_price = generate_target();
        $result2 = $mysqli->query("INSERT INTO `orders` (`id`, `start_datatime`, `end_datatime`, `last_update`, `ticker`, `start_price`, `target_price`, `order_stage`, `stop_loss`, `units`, `last_update_price`, `fallback`, `exit_price`, `avg_order`, `order_id`) VALUES (NULL, '$datetime', NULL, NULL, '$instrument', '$start_price', '$target_price', '$order_stage', NULL, '$units', '', NULL, '', NULL, '$order_id');");
        if ($result2 == true){
            echo "success";
        }else{
            echo "error";
        }


    }

    function check_stage() {
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `stratage`.`check_stage` LIMIT 0, 1000");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $order_stage = (int)$rows[0]['SUM(order_stage)'];
        if ($order_stage == '0'){$stage = '1';}elseif($order_stage == '1'){$stage = '2';}elseif($order_stage == '3'){$stage = '3';}elseif($order_stage == '6'){$stage = 'limit orders';}elseif($order_stage > '6'){$stage = 'limit';}
        return $stage;
    }

    function generate_trend(){
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `stratage`.`libor` LIMIT 0, 1000");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $usd_or_eur = $rows[0] ['usd_or_eur'];
        $up = '';
        $down = '-';
        if ($usd_or_eur == 'usd'){$trend = $down;}
        else{$trend = $up;}
        return $trend;
    }

    function generate_target(){
        global $mysqli;
        $r2 = 'r2';
        $s2 = 's2';
        if(generate_trend() == '-'){$get = $r2;}
        else{$get = $s2;}
        $result2 = $mysqli->query("SELECT * FROM `stratage`.`pivot` LIMIT 0, 1000");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows2[] = $result2->fetch_assoc();
        }
        $target = $rows2[0] [$get];
        return $target;
    }

    function close_orders(){
       //put https://api-fxpractice.oanda.com/v3/accounts/101-004-8911163-002/trades/5/close
//        body {
//            "units": "100"
//        }
    }

    function list_orders(){
//    get    https://api-fxpractice.oanda.com/v3/accounts/101-004-8911163-002/openTrades
    }

    function trail_sl(){}

    function take_profit(){}

    function account_info(){
        global $mysqli;
        $result = $mysqli->query("SELECT * FROM `configure`");
        //use mysqli->affected_rows
        for ($x = 1; $x <= $mysqli->affected_rows; $x++) {
            $rows[] = $result->fetch_assoc();
        }
        $account = account();
        $token = $rows[0] ['token'];
        $ch = curl_init();
        // Returns the data/output as a string instead of raw data
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //Set your auth headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $token
        ));
        curl_setopt($ch, CURLOPT_URL, "https://api-fxpractice.oanda.com/v3/accounts/" . $account . "/summary");
        // get stringified data/output. See CURLOPT_RETURNTRANSFER
        $data = curl_exec($ch);
        // get info about the request
        // close curl resource to free up system resources
        curl_close($ch);
        $obj = json_decode($data, true);
        $id = $obj['account']['id'];
        $balance = $obj['account']['balance'];
        $currency = $obj['account']['currency'];
        $marginAvailable = $obj['account']['marginAvailable'];
        $marginUsed = $obj['account']['marginUsed'];
        $marginRate = $obj['account']['marginRate'];
        $openPositionCount = $obj['account']['openPositionCount'];
        $openTradeCount = $obj['account']['openTradeCount'];
        $pendingOrderCount = $obj['account']['pendingOrderCount'];
        $pl = $obj['account']['pl'];
        $hedgingEnabled = $obj['account']['hedgingEnabled'];
        $marginCallPercent = $obj['account']['marginCallPercent'];
        $datetime = date('Y-m-d h:m:s', strtotime("Now"));
        global $mysqli;
        $result = $mysqli->query("UPDATE `stratage`.`account_info` SET `account_id` = '$id', `balance` = '$balance', `currency` = '$currency', `marginAvailable` = '$marginAvailable', `marginUsed` = '$marginUsed', `marginRate` = '$marginRate', `openPositionCount` = '$openPositionCount', `openTradeCount` = '$openTradeCount', `pendingOrderCount` = '$pendingOrderCount', `pl` = '$pl', `hedgingEnabled` = '$hedgingEnabled', `marginCallPercent` = '$marginCallPercent', `last_update` = '$datetime' WHERE `id` = 1");
        if ($result == true){
            echo "success";
        }else{
            echo "error";
        }
    }
?>